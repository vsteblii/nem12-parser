# Disclaimer
There are numerous areas for improvement in this code if we intend to deploy it in a production system. Enhancing unit test coverage is top priority to bolster confidence.

# Overview
The main goal of this project is to support reading of the Meter data file format (nem12) from CSV file and store nem12 records into DB with this columns: nmi, timestamp, consumption.
You can read a whole scecifcation of this format here 

The primary objective of this project is to facilitate the parsing of Meter Data File format (NEM12) from CSV files and then store NEM12 records into a database with columns including NMI, timestamp, and consumption. The detailed specification of this format can be found at [this link](https://aemo.com.au/-/media/files/electricity/nem/retail_and_metering/metering-procedures/2017/mdff_specification_nem12_nem13_final_v102.pdf)


# Assumption that have been made
1. Due to potentially large file sizes, relying solely on in-memory solutions is not going to work.
2. NMIs are not necessarily unique and may appear at various points within the CSV file. Therefore, I did not assume that all duplicates are stored consecutively, as this consideration also impacts technical decisions.
3. Presently, implementation lacks error handling for every scenario, and errors are simply thrown as exceptions when encountered. 
4. I am expecting that file is correct.
5. My assumption was that although NMIs may not be unique within the file, records with the same NMI+Timestamp should not already exist in the database, as this would also influence technical decisions.
6. Each record only includes KwH.

# What can be done better?
1. Unit test coverage should be imporoved.
2. Removing database constraints would significantly enhance the speed of bulk inserts into the database. (100 mb file ingested 3 times faster withour constraints)
3. The current implementation has been tested on a robust machine, but it may be necessary to adjust chunk sizes and the number of records bulk-inserted into the database for different environments.
4. If there's a necessity to adapt this code for additional NEM formats such as NEM13, restructuring the code is imperative. Ensuring certain portions of the code are more abstract will facilitate its adaptation for other formats, although premature abstraction might complicate code comprehension.
5. While statically typed languages such as Go or Rust are viable options, in this scenario, file processing takes a relatively small portion of the execution time. The bulk of the time is spent during the insertion process itself.
6. Instead of performing bulk inserts directly from JavaScript, the PostgreSQL COPY function offers an alternative approach. However, it comes with a tradeoff: we would have slightly less control over the insertion of records.
7. Typescript can be used to add types support. 
8. Other potential improvements may exist, but a better comprehension of data constraints is necessary to pursue them effectively.
9. If something really important goes wrong, we can't pick up from where we left off. We could save details about the records we've successfully handled in a file or a database, but we haven't set up that system yet.


# How to generate a fake NEM12 file
Run `node faker.js` to create a `fakeNEM12.csv` file in input folder. 

# How it works?
1. The primary script reads data from input/fakeNEM12.csv by default.
2. The script truncates the entire meter_readings table (this logic is unnecessary for production but useful for testing purposes).
3. Duplicate NMIs in the file are identified as they cannot be processed immediately.
4. The file is read in chunks, and when a complete valid NEM chunk (200 records and their children) is identified without duplicates, it can be processed and stored in the database.
5. Chunks containing duplicates are processed only after the last duplicate has been found in the file.
6. Database inserts are performed in chunks of 1,000 as it significantly improves insertion speed.

# Testing
Scrips have been tested on Apple M1 Pro/16 gb:
- [With DB constraint] It took 210 sec to process 100 mb of data ~= 13kk records in DB
- [Without DB constraint] It took 85 sec to process 100 mb of data ~= 13kk records
As data is parsed in chunks, memory consumption is stable. 


## Getting started
1. Make sure that you have Docker and NVM (NodeJS 20) installed
    1. `nvm use v20`
2. Copy `.env.example` content to `.env` and update variables (if needed) 
3. Update a path to a file in `./config.js`
4. Run `npm install`
5. Run `npm run docker:up` to start a DB and init DB with `./database/init.sql`
6. Run `node index.js` to read data from `./input/fakeNEM12.csv` and populate `meter_readings` DB table
