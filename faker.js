const fs = require('fs');
const { createNEM12CSVChunk, createNEM12CSVHeader, createNEM12CSVDataEnd, createNMIByIndex } = require('./src/utils/faker');
const config = require('./config');
const writeStream = fs.createWriteStream(config.inputFile.path);

const numberOfChunks = 1000;
const daysOfData = 5;

function generateFakeNem12Chunks(chunkIndex) {
    let nem12CSVstr = '\n' + createNEM12CSVChunk(createNMIByIndex(chunkIndex), 30, daysOfData, new Date()).join('\n');
    // just create a fake duplicated
    if (chunkIndex % 10 === 0) {
        nem12CSVstr += nem12CSVstr;
    }
    return nem12CSVstr;
}

let currentChunk = 0;

function writeToStream() {
    if (currentChunk === 0) {
        writeStream.write(createNEM12CSVHeader());
    }

    if (currentChunk++ == numberOfChunks) {
        writeStream.write(createNEM12CSVDataEnd());
        return;
    }

    const data = generateFakeNem12Chunks(currentChunk);
    const canContinue = writeStream.write(data);

    if (!canContinue) {
        writeStream.once('drain', writeToStream);
    } else {
        writeToStream();
    }
}

writeToStream();