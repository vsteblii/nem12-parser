require('dotenv').config();

const { displayMemoryUsage } = require('./src/utils/memory');
const { knex, truncateTable } = require('./src/knex');
const { createPerformanceTimer } = require('./src/utils/performance');
const { processNEM12File, readDuplicatedNMIFromFile } = require('./src/nem12File');
const config = require('./config');

const timer = createPerformanceTimer();

displayMemoryUsage();
const memoryInterval = setInterval(displayMemoryUsage, 5000);

async function main() {
    await truncateTable('meter_readings');
    timer('Time taken to truncate DB');

    const duplicated = await readDuplicatedNMIFromFile(config.inputFile.path);
    timer('Time taken to get duplicates');

    await processNEM12File(config.inputFile.path, duplicated);
    timer('Time taken to parse file');

    knex.destroy();
    clearTimeout(memoryInterval);
}

main();
