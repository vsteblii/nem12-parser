const HEADER_RECORD_CODE = 100;
const DATA_DETAILS_RECORD_CODE = 200;
const INTERVAL_DATA_RECORD_CODE = 300;
const INTERVAL_EVENT_RECORD_CODE = 400;
const B2B_DETAILS_RECORD_CODE = 500;
const END_OF_DATA_CODE = 900;

function isHeaderRecord(record){
    return record.startsWith(HEADER_RECORD_CODE);
}

function isDataDetailsRecord(record) {
    return record.startsWith(DATA_DETAILS_RECORD_CODE);
}

function isIntervalDataRecord(record) {
    return record.startsWith(INTERVAL_DATA_RECORD_CODE);
}

function isIntervalEventRecord(record) {
    return record.startsWith(INTERVAL_EVENT_RECORD_CODE);
}

function isB2BDetailsRecord(record) {
    return record.startsWith(B2B_DETAILS_RECORD_CODE);
}

function isEndOfDataRecord(record) {
    return record.startsWith(END_OF_DATA_CODE);
}

function convertStreamOutputToNEMChunksBuilder() {
    const result = {
        partialChunk: '',
        fullChunks: [],
    };
    return (chunk) => {
        const hasEndOfFileMarker = chunk.indexOf('\n900') !== -1;
        if (result.partialChunk.length === 0 && hasEndOfFileMarker) {
            // chunk is larger then file size
            return {
                ...result,
                fullChunks: [chunk],
            };
        }

        const fullChunks = [];
        if (result.partialChunk.length > 0) {
            // add new chunk at the end of partial chunk
            result.partialChunk += chunk;

            if(result.partialChunk.indexOf('\n900') !== -1){
                //end of the file
                return {
                    fullChunks: [result.partialChunk],
                    partialChunk: '',
                };
            }

            const firstNEM12ChunkStart = result.partialChunk.indexOf('\n200,');
            const lastNEM12ChunkStart = result.partialChunk.lastIndexOf('\n200,');
            // we have just one 200 blocks so it could be just part of the chunk
            if (firstNEM12ChunkStart === lastNEM12ChunkStart) {
                return result;
            }

            const fullChunk = result.partialChunk.slice(0, lastNEM12ChunkStart);
            fullChunks.push(fullChunk);
            result.partialChunk = result.partialChunk.slice(lastNEM12ChunkStart);
            return {
                ...result,
                fullChunks,
            };
        }

        const firstNEM12ChunkStart = chunk.indexOf('\n200,');
        const lastNEM12ChunkStart = chunk.lastIndexOf('\n200,');

        const two200BlocksInchunk = firstNEM12ChunkStart !== lastNEM12ChunkStart;
        if(two200BlocksInchunk){
            const fullChunk = chunk.slice(0, lastNEM12ChunkStart);
            fullChunks.push(fullChunk);
            result.partialChunk = chunk.slice(lastNEM12ChunkStart);
            return {
                ...result,
                fullChunks,
            };
        }


        if (firstNEM12ChunkStart === lastNEM12ChunkStart) {
            // we found just one 200 block in this chunk, 
            // it is not enough to judge if this is full chunk
            result.partialChunk += chunk;
            return result;
        }

        if (lastNEM12ChunkStart === -1) {
            // we don't have a 200 block, we just need to store partial chunk for now
            result.partialChunk += chunk;
            return result;
        }

        return result;
    };
}

module.exports = {
    isHeaderRecord,
    isDataDetailsRecord, 
    isIntervalDataRecord,
    isIntervalEventRecord,
    isB2BDetailsRecord,
    isEndOfDataRecord,
    convertStreamOutputToNEMChunksBuilder,
};