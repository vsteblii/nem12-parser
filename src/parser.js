const { isDataDetailsRecord, isB2BDetailsRecord, isIntervalEventRecord, isIntervalDataRecord, isHeaderRecord, isEndOfDataRecord } = require("./nem12");
const { convertCSVLineToArray } = require("./utils/csv");
const { nem12DateToJsDate, addMinutesShiftToDate } = require("./utils/date");

function parseDataDetailsRecord(record) {
    if (!isDataDetailsRecord(record)) {
        throw new Error('Invalid data details record');
    }

    const parsedRecord = convertCSVLineToArray(record);
    if (parsedRecord.length < 9) {
        throw new Error('Invalid format of details record');
    }
    return { nmi: parsedRecord[1], intervalLength: parseInt(parsedRecord[8]) };
}

function parseIntervalDataRecord(record, chunkValues, intervalLength = 30) {
    if (!isIntervalDataRecord(record)) {
        throw new Error('Invalid interval data record');
    }

    const parsedRecord = convertCSVLineToArray(record);
    const intervalValuesCount = 1440 / intervalLength;
    const metadataPrefixLength = 2;
    const valuableDataLength = metadataPrefixLength + intervalValuesCount;
    // required metadata + values count
    if (parsedRecord.length < valuableDataLength) {
        throw new Error('Invalid format of details record');
    }
    const initialIntervalDate = nem12DateToJsDate(parsedRecord[1]);

    for(let i = 2; i < valuableDataLength; i++){
        const currentTimeShift = intervalLength * (i-metadataPrefixLength);
        const intervalDate = addMinutesShiftToDate(initialIntervalDate, currentTimeShift);
        const previousValue = chunkValues.get(intervalDate) ?? 0;
        const currentValue = parseFloat(parsedRecord[i]);
        chunkValues.set(intervalDate.getTime(), previousValue + currentValue);
    }
}

// just ignore whitespaces in file, we can also throw an error but it depends on requirments
function isEmptyRecord(record){
    return record.length === 0;
}

function parseNEM12Chunk(records) {
    if (!records.length) {
        throw new Error('Chunk can\'t be empty, should contain 200-500 records');
    }

    const hasHeaderRecord = isHeaderRecord(records[0]);
    const chunkDetailsRecordIndex = hasHeaderRecord ? 1 : 0;
    const chunkDetails = parseDataDetailsRecord(records[chunkDetailsRecordIndex]);
    const chunkValues = new Map();

    for (let record of records) {
        if (isIntervalDataRecord(record)) {
            parseIntervalDataRecord(record, chunkValues, chunkDetails.intervalLength);
            continue;
        }

        if (isEmptyRecord(record) ||
            isB2BDetailsRecord(record) ||
            isIntervalEventRecord(record) ||
            isDataDetailsRecord(record) || 
            isHeaderRecord(record) ||
            isEndOfDataRecord(record)) {
            /**
             * Interval event record (400)
             * 
             * This record is mandatory where the QualityFlag is ‘V’ in the 300 record or where the quality flag is
             * ‘A’ and reason codes 79, 89, and 61 are used.
             * 
             * Code 400 is intentionally ignored in this codebase as it haven't been 
             * mentioned in examples and in assesment description. 
             */
            continue;
        }

        throw new Error('Invalid records type');
    }

    return {
        ...chunkDetails,
        values: chunkValues
    };
}

module.exports = {
    parseNEM12Chunk,
    parseDataDetailsRecord,
};