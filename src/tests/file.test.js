const { convertStreamOutputToNEMChunksBuilder } = require('../nem12');

const mock = `100,NEM12,200506081149,UNITEDDP,NEMMCO
200,N000000001,E1E2,1,E1,N1,01009,kWh,30,20240309
300,20240311,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,A,,,20050310121004,20050310182204
300,20240312,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,A,,,20050310121004,20050310182204
500,O,S01009,20240315121004,
200,N000000002,E1E2,1,E1,N1,01009,kWh,30,20240309
300,20240311,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,A,,,20050310121004,20050310182204
300,20240312,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,A,,,20050310121004,20050310182204
500,O,S01009,20240315121004,
200,N000000003,E1E2,1,E1,N1,01009,kWh,30,20240309
300,20240311,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,A,,,20050310121004,20050310182204
300,20240312,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,A,,,20050310121004,20050310182204
500,O,S01009,20240315121004,
900`;

describe('convertStreamOutputToChunks', () => {
    it('should store values in partialChunk when we can\'t get a whole valid chunk at once', () => {
        const defaultChunkLength = 100;
        const expectedResult = [
            {
                // we haven't seen yet all children of 200 record
                partialChunk: mock.slice(0, 100),
                fullChunks: [],
            },
            {
                // we haven't seen yet all children of 200 record
                partialChunk: mock.slice(0, 200),
                fullChunks: [],
            },
            {
                // we haven't seen yet all children of 200 record
                partialChunk: mock.slice(0, 300),
                fullChunks: [],
            },
            {
                // we haven't seen yet all children of 200 record
                partialChunk: mock.slice(0, 400),
                fullChunks: [],
            },
            {
                // first time when really have one full chunk
                partialChunk: mock.slice(481, 500),
                fullChunks: [
                    mock.slice(0, 481)
                ],
            },
            {
                partialChunk: mock.slice(481, 600),
                fullChunks: [],
            },
            {
                partialChunk: mock.slice(481, 700),
                fullChunks: [],
            },
            {
                partialChunk: mock.slice(481, 800),
                fullChunks: [],
            },
            {
                partialChunk: mock.slice(481, 900),
                fullChunks: [],
            },
            {
                partialChunk: mock.slice(924, 1000),
                fullChunks: [
                    mock.slice(481, 924)
                ],
            },
            {
                partialChunk: mock.slice(924, 1100),
                fullChunks: [],
            },
            {
                partialChunk: mock.slice(924, 1200),
                fullChunks: [],
            },
            {
                partialChunk: mock.slice(924, 1300),
                fullChunks: [],
            },
            {
                partialChunk: '',
                fullChunks: [mock.slice(924, 1400)],
            },
        ];

        const convertStreamOutputToChunks = convertStreamOutputToNEMChunksBuilder();
        for (let i = 0; i < expectedResult.length; i++) {
            const start = i * defaultChunkLength;
            const mockChunk = mock.slice(start, start + defaultChunkLength);
            const result = convertStreamOutputToChunks(mockChunk);
            expect(result).toEqual(expectedResult[i]);
        }
    });
    
    it('should store values in fullchunk when we can get a whole valid chunk at once', () => {
        const defaultChunkLength = 500;
        const expectedResult = [
            {
                partialChunk: mock.slice(481, 500),
                fullChunks: [
                    mock.slice(0, 481)
                ],
            },
            {
                partialChunk: mock.slice(924, 1000),
                fullChunks: [
                    mock.slice(481, 924)
                ],
            },
            {
                partialChunk: '',
                fullChunks: [
                    mock.slice(924, 1400)
                ],
            },
        ];

        const convertStreamOutputToChunks = convertStreamOutputToNEMChunksBuilder(defaultChunkLength);
        for (let i = 0; i < expectedResult.length; i++) {
            const start = i * defaultChunkLength;
            const mockChunk = mock.slice(start, start + defaultChunkLength);
            const result = convertStreamOutputToChunks(mockChunk);
            expect(result).toEqual(expectedResult[i]);
        }
    });

    it('should handle properly when chunk is larger then a file size', () => {
        const defaultChunkLength = 1500;
        const expectedResult = [
            {
                partialChunk: '',
                fullChunks: [
                    mock.slice(0, 1500)
                ],
            },
        ];

        const convertStreamOutputToChunks = convertStreamOutputToNEMChunksBuilder(defaultChunkLength);
        for (let i = 0; i < expectedResult.length; i++) {
            const start = i * defaultChunkLength;
            const mockChunk = mock.slice(start, start + defaultChunkLength);
            const result = convertStreamOutputToChunks(mockChunk);
            expect(result).toEqual(expectedResult[i]);
        }
    });
});