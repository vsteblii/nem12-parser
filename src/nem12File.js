const readline = require('readline');
const fs = require('fs');
const path = require('path');
const { isHeaderRecord, isDataDetailsRecord, isEndOfDataRecord, convertStreamOutputToNEMChunksBuilder } = require('./nem12');
const { parseNEM12Chunk, parseDataDetailsRecord } = require('./parser');
const { readFileChunk } = require('./file');
const { batchInsert } = require('./knex');

function prepareChunkDataForBatchInsert(chunks) {
    const chunksArr = [];
    for (const [key, value] of chunks) {
        const nmiData = value;
        for (const [timestamp, value] of nmiData.values.entries()) {
            chunksArr.push({
                nmi: nmiData.nmi,
                timestamp: new Date(timestamp),
                consumption: value
            });
        }
    }
    return chunksArr;
}

async function readDuplicatedNMIFromFile(filePath) {
    return new Promise((resolve, reject) => {
        const chunksOccurance = new Map();
        const rl = readline.createInterface({
            input: fs.createReadStream(filePath)
        });

        rl.on('line', (line) => {
            if (!isDataDetailsRecord(line)) {
                return;
            }
            const recordData = parseDataDetailsRecord(line);
            const chunkOccurance = chunksOccurance.get(recordData.nmi) ?? null;
            chunksOccurance.set(recordData.nmi, chunkOccurance ? chunkOccurance + 1 : 1);
        });

        rl.on('close', () => {
            resolve(filterDuplicatedNMI(chunksOccurance));
        });

        rl.on('error', (error) => {
            reject(error);
        });
    });
}

function filterDuplicatedNMI(nmiMap) {
    const filteredMap = new Map();
    for (const [key, value] of nmiMap.entries()) {
        if (value > 1) {
            filteredMap.set(key, value);
        }
    }
    return filteredMap;
}

function mergeChunks(chunkData, newChunkData) {
    const mergedObject = { ...chunkData };

    for (const [key, value] of newChunkData.values.entries()) {
        if (mergedObject.values.has(key)) {
            mergedObject.values.set(key, mergedObject.values.get(key) + value);
        } else {
            mergedObject.values.set(key, value);
        }
    }
    return mergedObject;
};

let parsedDuplicatedChunks = new Map();
function parseFullNEM12StrChunks(chunks, duplicatedNMI) {
    const parsedUniqueChunks = new Map();
    for (let i = 0; i < chunks.length; i++) {
        const preparedChunkArr = chunks[i].split('\n').filter(c => c.length);
        const parsedChunk = parseNEM12Chunk(preparedChunkArr);
        const nmi = parsedChunk.nmi;
        if (duplicatedNMI.has(nmi)) {
            if (parsedDuplicatedChunks.has(nmi)) {
                const aggregatedChunkData = parsedDuplicatedChunks.get(nmi);
                const mergedChunkData = mergeChunks(aggregatedChunkData, parsedChunk);

                // decrease a duplication counter or remove value from duplication array
                let duplicationCounter = duplicatedNMI.get(nmi) - 1;
                // no duplicates left
                if (duplicationCounter == 1) {
                    parsedUniqueChunks.set(nmi, mergedChunkData);
                    parsedDuplicatedChunks.delete(nmi);
                    duplicatedNMI.delete(nmi);
                } else {
                    parsedDuplicatedChunks.set(nmi, mergedChunkData);
                    duplicatedNMI.set(nmi, duplicationCounter);
                }
            } else {
                parsedDuplicatedChunks.set(nmi, parsedChunk);
            }
        } else {
            parsedUniqueChunks.set(nmi, parsedChunk);
        }
    }

    return parsedUniqueChunks;
}

function nem12strToNEM12arr(chunkStr) {
    const records = [];
    let currentRecord = [];

    const lines = chunkStr.split('\n');

    for (const line of lines) {
        const parts = line.trim().split(',');
        if (parts[0] === '200') {
            if (currentRecord.length > 0) {
                records.push(currentRecord.join('\n'));
                currentRecord = [];
            }
            currentRecord.push(line.trim());
        } else if (['300', '500'].includes(parts[0])) {
            currentRecord.push(line.trim());
        } else if (parts[0] === '900') {
            if (currentRecord.length > 0) {
                records.push(currentRecord.join('\n'));
            }
            break;
        }
    }
    return records;
}

async function processNEM12File(filePath, duplicated) {
    const convertStreamOutputToChunks = convertStreamOutputToNEMChunksBuilder();

    let chunkLength = 10000;
    let fullChunks = [];
    let start = 0;

    const startIndexInterval = setInterval(() => console.log(start), 5000);

    while (true) {
        const chunk = await readFileChunk(filePath, start, start + chunkLength);
        const parsedChunks = convertStreamOutputToChunks(chunk);
        if (parsedChunks.fullChunks.length) {
            fullChunks = fullChunks.concat(parsedChunks.fullChunks)
        }
        start = start + chunkLength + 1;

        const endOfFile = chunk.length === 0;

        if (fullChunks.length > 10 || endOfFile) {
            const nem12Chunks = nem12strToNEM12arr(fullChunks.join(''));
            fullChunks = [];
            const parsedChunks = parseFullNEM12StrChunks(nem12Chunks, duplicated);
            const preparedInsertData = prepareChunkDataForBatchInsert(parsedChunks);
            try {
                await batchInsert('meter_readings', preparedInsertData);
            } catch (err) {
                console.log(err);
                // need to understand better what is the use case
                // here and how do we want to resolde DB issues
            }
        }

        if (endOfFile) {
            break;
        }
    }

    clearTimeout(startIndexInterval);
}

module.exports = {
    processNEM12File,
    readDuplicatedNMIFromFile,
};