const config = {
    client: 'pg',
    connection: {
        host: process.env.POSTGRES_HOST,
        user: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_DB,
    }
};

const knex = require('knex')(config);

async function truncateTable(tableName = 'meter_readings') {
    return knex(tableName)
        .truncate()
        .then(() => {
            console.log(`${tableName} table truncated successfully.`);
        })
        .catch((error) => {
            console.error(`Error truncating ${tableName} table:`, error);
        });
}

async function batchInsert(tableName, preparedInsertData, chunkSize = 20000) {
    /**
     * I have compared this batchInsert with raw SQL generation 
     * and there is almost no diff is terms of speed
     */
    return knex.batchInsert(tableName, preparedInsertData, chunkSize)
        .returning('id')
        .catch(function (error) {
            // TODO what do we really want to do in case of DB error?
            // do we want to stop executions and store last inserter index? 
            console.log(error);
        });
}

module.exports = {
    knex,
    truncateTable,
    batchInsert,
};
