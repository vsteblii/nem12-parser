function convertCSVLineToArray(csvLine) {
    return csvLine.split(',');
}

module.exports = {
    convertCSVLineToArray,
};