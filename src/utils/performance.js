function createPerformanceTimer(){
    const startTime = performance.now();
    return (message) => {
        console.log(message + ':', performance.now() - startTime, 'milliseconds');
    }
}

module.exports = {
    createPerformanceTimer,
};