const { dateToNEM12, addMinutesShiftToDate } = require("./date");

function createFakeIntervalDataRecord(date = new Date(), interval = 30, startingCounter = 1) {
    const minutesInDay = 1440;
    const intervalsLength = minutesInDay / interval;
    const fakeIntervalData = Array.from({ length: intervalsLength }, (_, index) => {
        return index + startingCounter;
    });
    const new12Date = dateToNEM12(date);
    return `300,${new12Date},${fakeIntervalData.join(',')},A,,,20050310121004,20050310182204`;
}

let startingCounter = 0;
function createFakeIntervalDataRecords(startDate = new Date(), daysOfData = 1, interval = 30) {
    startingCounter += 1;
    return Array.from({ length: daysOfData }, (_, index) => {
        const shift = 1440 * index;
        const data = addMinutesShiftToDate(startDate, shift);
        return createFakeIntervalDataRecord(data, interval, startingCounter)
    });
}

function createNEM12CSVHeader() {
    return `100,NEM12,200506081149,UNITEDDP,NEMMCO`;
}

function createNEM12CSVDataEnd() {
    return `\n900`;
}

function createNMIByIndex(index) {
    const paddedIndex = String(index).padStart(9, '0');
    return 'N' + paddedIndex;
}

function createNEM12CSVChunk(nmi, intervalLength, daysOfData, startDate = new Date()) {
    const intervalDataRecords = createFakeIntervalDataRecords(startDate, daysOfData, intervalLength);
    return [
        `200,${nmi},E1E2,1,E1,N1,01009,kWh,${intervalLength},20240309`,
        ...intervalDataRecords,
        '500,O,S01009,20240315121004,'
    ];
}

module.exports = {
    createFakeIntervalDataRecord,
    createNEM12CSVChunk,
    createNEM12CSVHeader,
    createNEM12CSVDataEnd,
    createNMIByIndex,
};