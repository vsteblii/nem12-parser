function displayMemoryUsage() {
    const memoryUsage = process.memoryUsage();
    console.log('Memory usage (MB):', {
        rss: memoryUsage.rss / (1024 * 1024),
        heapTotal: memoryUsage.heapTotal / (1024 * 1024),
        heapUsed: memoryUsage.heapUsed / (1024 * 1024),
        external: memoryUsage.external / (1024 * 1024)
    });
}

module.exports = {
    displayMemoryUsage,
};