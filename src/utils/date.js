// i just include this helper fuctions to avoid adding date libraries
function dateToNEM12(date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}${month}${day}`;
}

function nem12DateToJsDate(dateString) {
    const year = dateString.slice(0, 4);
    const month = dateString.slice(4, 6) - 1; 
    const day = dateString.slice(6, 8);

    const date = new Date(year, month, day);
    return date.getTime();
}

function addMinutesShiftToDate(date, shiftInMinutes) {
    const newDate = new Date(date);
    newDate.setMinutes(newDate.getMinutes() + shiftInMinutes);
    return newDate
}

module.exports = {
    dateToNEM12,
    nem12DateToJsDate,
    addMinutesShiftToDate,
};