const fs = require('fs');

function readFileChunk(filePath, start, end) {
    return new Promise((resolve, reject) => {
        const readStream = fs.createReadStream(filePath, { start, end });
        let data = '';

        readStream.on('data', (chunk) => {
            data += chunk;
        });

        readStream.on('error', (err) => {
            reject(err);
        });

        readStream.on('end', () => {
            resolve(data);
        });
    });
}


module.exports = {
    readFileChunk,
};